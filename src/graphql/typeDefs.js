const AddOnsTypeDefs = require('./AddOns/typeDefs');
const JsonType = 'scalar JSON';

const baseTypeDefs = `
type UserPermission {
    id: String
    operations: [String]
}

type User {
    userId: String
    active: Boolean
    email: String
    firstName: String
    lastName: String
    username: String
    permissions: [UserPermission]
}

type Query {
    hello: String
    verifyToken(token: String!, correlationId: String): User
}

type Mutation {
    hello: String
}

`;

const typeDefs = [
  AddOnsTypeDefs,
  baseTypeDefs
];

module.exports = typeDefs;
