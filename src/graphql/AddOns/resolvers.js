const getAddOnsCheckoutSummary = require('./getAddOnsCheckoutSummary');
const resolvers = {
  Query: {
    getAddOnsCheckoutSummary
  },
};

module.exports = resolvers;
