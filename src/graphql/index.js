/* eslint-disable no-console,max-lines */
const get = require('lodash.get');
const express = require('express');
const { ApolloServer } = require('@apollo/server');
const cors = require ('cors');
const serverlessExpress = require ('@codegenie/serverless-express');
const { expressMiddleware } = require ('@apollo/server/express4');
const { ApolloServerPluginLandingPageLocalDefault } = require('apollo-server-core');
const {
    settings: {
        START_LOCAL,
        LOCAL_HOST_PORT
    } = {},
} = require('../config');

// Provide resolver functions for your schema fields
const resolvers = require('./resolvers');
// Construct a schema, using GraphQL schema language
const typeDefs = require('./typeDefs');

const server = new ApolloServer({
    typeDefs,
    resolvers,
    csrfPrevention: true,
    plugins: [
        ApolloServerPluginLandingPageLocalDefault({ embed: true }),
    ]
});

server.startInBackgroundHandlingStartupErrorsByLoggingAndFailingAllRequests();

const verifyJwt = async ({ authToken }) => {
    //use firebase to verify jtw
}

const app = express();
app.use(
    cors({
        origin: true
    }),
    express.json(),
    expressMiddleware(server, {
        context: async({ req }) => {
            let base = {};
            try {
                await db.variable.init;
                const authToken = req.headers.authorization;

                // null is stringified when pulling from local storage in apolloClient
                if ((!authToken || authToken === 'null')) {
                    return {
                        ...base
                    };
                }

                if (authToken) {
                    const claims = await verifyJwt({ authToken });
                    let profileId = get(claims, 'user_metadata.profileId'); // Irrelevant when "login as member profile" is used
                    const dashboardContext = {
                        // Important for Dashboard Use
                        profileId,
                        user: claims,
                    };

                    return {
                        profile: await getProfile(profileId),
                        profileId,
                        ...base,
                        ...dashboardContext,
                    };
                }
                return base; // This is never reached but it makes the linter happy
            } catch (err) {
                console.error('Error creating context ', err.message, err.stack);
            }
            return base;
        }
    })
);

if ((process.env.NODE_ENV === 'LOCAL' || START_LOCAL) && !process.env.AWS_LAMBDA_FUNCTION_NAME) {
    const port = LOCAL_HOST_PORT || 8080;
    app.listen(port, () => {
        console.log(); // add blank line
        console.log(`🛩️  Surf Air API is listening at http://localhost:${port}`);
    });
}

module.exports = serverlessExpress({ app });
