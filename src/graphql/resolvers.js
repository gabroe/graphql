const AddOnsResolvers = require('./AddOns/resolvers');

const hello = () => 'Hello World!';
const baseResolvers = {
  Query: {
    hello
  },
  Mutation: {
    hello
  }
};

const resolvers = [
  baseResolvers,
  AddOnsResolvers,
];

module.exports = resolvers;
