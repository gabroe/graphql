const { gql } = require('apollo-server-lambda');

module.exports = gql`
type AddOnCheckoutLineItem {
    _id: String!
    type: String!
    label: String!
    infoMessage: String
    amount: Int!
    itemization: [Itemization]
}

type Itemization {
    label: String
    amount: Int
}

type AddOnsCheckoutSummary {
    checkoutLineItems: [AddOnCheckoutLineItem]
}

input AddOnsCheckoutSummaryInput {
    credits: Int
    destinationMemberships: Int
    baseMemberships: Int
    membershipInterval: String
}
input AddOnsMarketplaceInput {
    credits: Int
    destinationMemberships: Int
    baseMemberships: Int
    membershipInterval: String
}

extend type Query {
    getAddOnsCheckoutSummary(params: AddOnsCheckoutSummaryInput):AddOnsCheckoutSummary
}

extend type Mutation {
    processAddOnsMarketplace(params: AddOnsMarketplaceInput): JSON
}
`;
